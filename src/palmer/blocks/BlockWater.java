package palmer.blocks;

import java.awt.image.BufferedImage;

import palmer.main.Main;

public class BlockWater extends Block {
public BufferedImage Texture1;
public static int WaterTexTimer;
public static int WaterFlowTimer;
public int waterCount = 2;
public BufferedImage Texture2;
public int texture = 1;
	public BlockWater(int id2) {
		super(id2);
		Texture1 = Main.Textures[6];
		Texture = Texture1;
		Texture2 = Main.Textures[36];
	}
	public void swap(){
		if(texture == 1){
			Texture = Texture2;
			texture = 2;
		}else if(texture == 2){
			Texture = Texture1;
			texture = 1;
		}
	}

}
