package palmer.gui;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Info extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel bugs = new JLabel("Bugs:");
	JLabel bugs1  = new JLabel("1.Can't Break Slimes");
	JLabel bugs2  = new JLabel("2.Can't Break Doors");
	JLabel bugs3  = new JLabel("3.Can't Break Blocks under Water");
	JLabel bugs4  = new JLabel("4.Can't Break Blocks next to water with a block under the water");
	JLabel bugs5 = new JLabel("2.Slime noclip other slimes");
	JLabel bugs6  = new JLabel("3.Slimes don't get hurt by nukes");
	JLabel bugs7  = new JLabel("4.InstaTree and Doors canont be used near the top");
	JLabel bugs8  = new JLabel("5.Block have to be clicked in the exact center");
	JLabel bugs9  = new JLabel("6.(Not Realy a bug)Slimes ignore gravity");
	
	
	public Info(){
		setTitle("Bug List");
		setResizable(false);
		setSize(400, 500);
		setLayout(new FlowLayout());
		add(bugs);
		add(bugs1);
		//add(bugs2);
		//add(bugs3);
		//add(bugs4);
		add(bugs5);
		add(bugs6);
		add(bugs7);
		add(bugs8);
		add(bugs9);
		//setVisible(true);
	}

}