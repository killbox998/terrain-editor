package palmer.items;

import palmer.exceptions.IncorrectItemException;
import palmer.main.Main;

public class ItemStack {
	public Item item;
	public int id;
	public int amount;
	public ItemStack(Item i){
		item = i;
	}
	public ItemStack(Item i, int a){
		item = i;
		amount = a;
		id = item.id;
	}
	public void add(Item i) throws IncorrectItemException{
		if(i.id == id){
			amount++;
		}else
			Main.rloop.con = "[Error]InvalidItemException";
		
	}
	public void add(Item i, int c) throws IncorrectItemException{
		if(i.id == id){
			amount+= c;
		}else
			Main.rloop.con = "[Error]InvalidItemException";
		
	}
	public Item take(){
		if(amount > 0){
			amount--;
			return item;
			
		}else
		return Item.test;
	}


}
