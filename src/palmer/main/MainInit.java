package palmer.main;

import java.io.IOException;

import palmer.blocks.Block;
import palmer.gui.Console;
import palmer.items.Item;
import palmer.res.Audio;
import palmer.res.CombineType;
import palmer.res.LoadImages;

public class MainInit {
	static LoadImages li = new LoadImages();
	public static void init(){
		Audio.playSound("Beginning");
		try {
			//loads block texture sheet
			Main.sheet = Main.li.getOutTex("/Terrain_8x");
			//loads item texture sheet
			Main.itemSheet = Main.li.getOutTex("/Items_8x");
			//loads menu texture sheet
			Main.menuSheet = Main.li.getOutTex("/Menu_8x");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//create some textures
		TexInit();
		//create all the block objects
		Block.init();
		Main.rloop = new Render();
		Main.MenuTextures = li.getBlocks(Main.menuSheet, li.Menu_Color);
		Main.ItemTextures = li.getBlocks(Main.itemSheet, li.Item_Color);
		for(int count = 0;count < 64;count++){
			Main.MenuTextures[count] = li.resize(Main.MenuTextures[count], 32, 32);
			Main.ItemTextures[count] = li.resize(Main.ItemTextures[count], 32, 32);
			
			
		}
		Item.init();
		Main.rloop.pm.init();
		//init console object
		Main.con = new Console();
		//men = new Menu();
		
	}
	static void TexInit() {
		Main.Textures = li.getBlocks(Main.sheet, li.Tex_Color);
		
		
		
		for(int count = 0;count < 64;count++){
			Main.Textures[count] = li.resize(Main.Textures[count], 32, 32);
			
			
			
			
		}
		li.combine(CombineType.BrickStairsR);
		li.combine(CombineType.BrickStairsL);
		li.combine(CombineType.BricksInside);
		li.combine(CombineType.BrickDoorL);
		li.combine(CombineType.BrickDoorR);
		li.combine(CombineType.Ladder);
		li.combine(CombineType.Lava);
		li.combine(CombineType.StoneInside);
		li.combine(CombineType.IronInside);
		li.combine(CombineType.GoldInside);
		li.combine(CombineType.DiamondInside);
		}


}
