package palmer.main;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;

import palmer.blocks.Block;
import palmer.gui.*;
import palmer.items.Item;
import palmer.levels.Save;
import palmer.res.Audio;
import palmer.res.CombineType;
import palmer.res.LoadImages;
import palmer.tools.Tool;

public class Main implements Runnable {
//Main Thread
public static Thread GameLoop = new Thread(new Main());
//Render Object
public static Render rloop;
//Input Handler
public static InputHandler in;
public static int screenClear = 0;
public static int water = 0;
//Console object
public static Console con;
//Current Gamemode
public static GameMode gm = GameMode.c;
//if the map can be edited or not
public static boolean Mapchanges = true;
//object for loading and editing images
public static LoadImages li = new LoadImages();
// variable for sleep beween iterations
public static int lag = 15;
// block texture array;
public static BufferedImage[] Textures;
//*Glitchy saving system
public static Save saveData[][] = new Save[64][3];
public static int currentSaveX = 31;
public static int currentSaveY = 1;
public static int cSX = 0;
public static int cSY = 0;
// Item Texture array
public static BufferedImage[] ItemTextures;
//Menu Texture array
public static BufferedImage[] MenuTextures;
//Texture sheet for blocks
public static BufferedImage sheet;
//Texture sheet for items
public static BufferedImage itemSheet;
//Texture Sheet for menus
public static BufferedImage menuSheet;
//unused
public static boolean Floodflow = false;
//unused
//public static Menu men;
//is true if in the first interation for setup
public static boolean firststart = true;
//Unused system for loading alternate textures in the old texture system
public static String TP = "";
//if game is running it is false
static boolean gstop = false;
//fuse of bomb
//if true plays sounds
public static boolean playSounds = true;
//if true updates whole screen
public static  boolean clear = true;
	@SuppressWarnings("static-access")
		public void run() {
		MainInit.init();
		//create all the item objects
		//fps(frames per second) counter
		int fps = 0;
		//init render object
		
		boolean gstop = false;
		//init the level
		rloop.lv1.init();
		long oldtime = System.currentTimeMillis();
		//men.init();
		Main.rloop.lv1.player.i.addItem(Tool.Wand, 9000);
		con.print("Program Started");
		rloop.con = "Program Started";
		while(!gstop){
			//Audio.update();
			rloop.Rendering();
			//men.Render();
			firststart = false;
			try {
				GameLoop.sleep(lag);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
	if(System.currentTimeMillis() - oldtime >= 1000){
		oldtime = System.currentTimeMillis();
		System.out.println(fps);
		if(fps > 40){
			lag++;
		}else if(fps < 20 && !(lag <= 0) ){
			lag--;
			if(fps < 10){
				lag = 0;
			}
		}
		fps = 0;
		
		screenClear++;
		water++;
		
			rloop.lv1.waterUpdate = true;
		
		if(screenClear == 5){
			screenClear = 0;
			clear = true;
		}
	}else{
		fps++;
	}
		}
	}
	public static void main(String[] args) {
	      GameLoop.start();

	}
	
	}
