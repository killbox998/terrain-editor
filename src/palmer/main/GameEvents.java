package palmer.main;

import palmer.blocks.Block;
import palmer.entity.TileEntity;
import palmer.entity.ToolChest;
import palmer.res.Audio;

public class GameEvents {
	public static boolean fire(String event){
		switch(event){
		case "nuke":
			Main.rloop.lv1.nuke();
			return true;
		case "removeWater":
			Main.rloop.lv1.waterRemove();
			return true;
		case "removeLava":
			Main.rloop.lv1.lavaRemove();
			return true;
		case "clear":
			Main.clear = true;
			return true;
		case "warpPad":
			TileEntity.WarpPad.activate();
			return true;
		case "radio":
			TileEntity.Radio.activate();
			return true;
		case "endWarpPad":
			TileEntity.WarpPad.deactivate();
			return true;
		case "endRadio":
			TileEntity.Radio.deactivate();
			return true;
		
		case "allowMapChanges":
			Main.Mapchanges = true;
			return true;
		case "banMapChanges":
			Main.Mapchanges = false;
			return true;
		case "playSong":
			Audio.playRandomSong();
			return true;
		case "stopSound":
			Audio.stopCurrentSound();
			Audio.removeAllSounds();
			return true;
		case "reset":
			Main.rloop.lv1.reset();
			return true;
		case "stopGame":
			Main.gstop = true;
			System.exit(0);
			return true;
		
		
		
		}
		return false;
	}
	public static boolean fire(String s, int x, int y){
		switch(s){
		case"instaTree":
			Main.rloop.lv1.instaTree(x, y);
			return true;
		case"instaHouse":
			Main.rloop.lv1.instaHouse(x, y);
			return true;
		case"instaCloud":
			Main.rloop.lv1.instaCloud(x, y);
			return true;
		case"antiWater":
			Main.rloop.lv1.removeWater(x, y);
			return true;
		
		
		}
		return false;
	}

}
