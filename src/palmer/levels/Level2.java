package palmer.levels;

import palmer.blocks.*;
import palmer.main.Main;
import static palmer.main.Main.rloop;

public class Level2 {
	static Block blocks[][] = new Block[17][17];
	public static void init() {
		rloop.lv1.player.y = 14*32;
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
					rloop.lv1.changeBlock(x, y, Block.air);
			}
		}
		for (int x = 0; x < 16; x++) {
			Main.rloop.lv1.changeBlock(x, 15, Block.stone);

		}
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				if (rloop.lv1.blocks[x][y] == null)
					rloop.lv1.changeBlock(x, y,Block.air);
			}
		}
	}

}
