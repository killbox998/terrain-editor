package palmer.levels;

import java.util.Random;

public class LevelOptions {
	public LevelType level;
	public boolean hasTree = false;
	public boolean hasOres = false;
	public boolean hasDiamond = false;
	public Boolean hasGold = false;
	public boolean hasIron = false;
	public boolean hasHouse = false;
	public boolean hasCave = false;
	public boolean hasCloud = false;
	public LevelOptions(LevelType lt) {
	level = lt;
	}

	public static LevelOptions getOptions(LevelType lt){
		LevelOptions lo = new LevelOptions(lt);
		switch(lt){
		case Cave:
		
			if(new Random().nextInt()%2 == 0){
				lo.hasOres = true;
				lo.hasIron = true;
				if(new Random().nextInt()%2 == 0){
					lo.hasDiamond = true;
				}
				if(new Random().nextInt()%2 == 0){
					lo.hasGold = true;
				}
				
				
			}else{
				lo.hasOres = false;
			}

				lo.hasTree = new Random().nextBoolean();
		
			if(new Random().nextInt()%2 == 0){
				lo.hasTree = true;
		}
			break;
		case Normal:
			lo.hasCloud = new Random().nextBoolean();
			if(new Random().nextInt()%2 == 0){
			if(new Random().nextInt()%2 == 0){
				lo.hasHouse = true;
			}else{
				
				lo.hasTree = true;
				
				
			}
			
			}else
				lo.hasCave = true;
			break;
			
		
		
		
		}
		return lo;
	}

}
