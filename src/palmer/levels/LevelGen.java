package palmer.levels;

import java.util.Random;

import palmer.blocks.Block;
import palmer.main.Main;
import palmer.main.Orentation;

public class LevelGen {
	public static GameRules getNewLevel() {
		return new GameRules() {
			public GameRules init(LevelType lt) {
				LevelOptions lo = null;
				if(Main.saveData[Main.currentSaveX + Main.cSX][Main.currentSaveY + Main.cSY] != null){
					lo = Main.saveData[Main.currentSaveX + Main.cSX][Main.currentSaveY + Main.cSY].lo;
					if( Main.saveData[Main.currentSaveX + Main.cSX][Main.currentSaveY + Main.cSY].lo != null)
					lt = Main.saveData[Main.currentSaveX + Main.cSX][Main.currentSaveY + Main.cSY].lo.level;
				}
				switch (lt) {
				case Normal:
					if(lo == null)
					lo = LevelOptions.getOptions(LevelType.Normal);
					CurrentLevelType = LevelType.Normal;
					cust_Air = Block.air;
					Main.li.fixStairs();
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][15] = Block.stone;
					}
					super.init();
					Main.rloop.lv1.player.x = 4 * 32;
					Main.rloop.lv1.player.y = 12 * 32;
					if(lo.hasTree){
						instaTree(12, 12);
					}else if(lo.hasHouse){
						instaHouse(9,12);
					}
					if(lo.hasCloud){
						instaCloud(6,3);
					}
					if(lo.hasCave)
					instaCave(11, 15);
					break;
				case Cave:
					if(lo == null)
					CurrentLevelType = LevelType.Cave;
					lo = LevelOptions.getOptions(LevelType.Cave);
					super.menInit();
					cust_Air = Block.stoneInside;
					Main.li.fixStairs();
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}

					for (int y = 1; y < 15; y++) {
						for (int x = 1; x < 15; x++) {
							blocks[x][y] = Block.stoneInside;
						}
					}
					if (lo.hasOres) {
						if(lo.hasIron){
						for (int y = 0; y < 5; y++) {
							blocks[new Random().nextInt(16)][new Random()
									.nextInt(16)] = Block.oreInside[Block.ORE_IRON];
						}
						}
						if (lo.hasGold) {
							for (int y = 0; y < 5; y++) {
								blocks[new Random().nextInt(16)][new Random()
										.nextInt(16)] = Block.oreInside[Block.ORE_GOLD];
							}
						}
						if (lo.hasDiamond) {
							for (int y = 0; y < 5; y++) {
								blocks[new Random().nextInt(16)][new Random()
										.nextInt(16)] = Block.oreInside[Block.ORE_DIAMOND];
							}
						}
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][15] = Block.stone;
					}
					// super.textureInit();
					for (int x = 0; x < 16; x++) {
						blocks[x][0] = Block.stone;
					}
					for (int y = 0; y < 16; y++) {
						blocks[0][y] = Block.stone;
					}
					for (int y = 0; y < 16; y++) {
						blocks[15][y] = Block.stone;
					}
					if(lo.hasHouse){
						instaHouse(8,14);
					}
					
					break;
				case Sand:
					super.menInit();
					cust_Air = Block.air;
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}
					for(int count = 0;count < 16;count++){
						blocks[count][15] = Block.sand;
					}
					for(int count = 0;count < 16;count++){
						blocks[count][14] = Block.sand;
					}
					
					if(new Random().nextBoolean()){
					blocks[10][13] = Block.cactus;
					blocks[10][12] = Block.cactus;
					blocks[10][11] = Block.cactus;
					blocks[10][10] = Block.cactus;
					blocks[10][9] = Block.cactus;
					}else{
						instaTree(10, 13, 0);
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					break;
				}
				Main.saveData[Main.currentSaveX + Main.cSX][Main.currentSaveY + Main.cSY] = new Save(lo);
				Main.cSX = 0;
				Main.cSY = 0;
				return this;
			}
			
			public GameRules init() {
				LevelOptions lo = null;
				switch (LevelGen.getRandType()) {
				case Normal:
					lo = LevelOptions.getOptions(LevelType.Normal);
					CurrentLevelType = LevelType.Normal;
					cust_Air = Block.air;
					Main.li.fixStairs();
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][15] = Block.stone;
					}
					super.init();
					Main.rloop.lv1.player.x = 4 * 32;
					Main.rloop.lv1.player.y = 12 * 32;
					if(lo.hasTree){
						instaTree(12, 12);
					}else if(lo.hasHouse){
						instaHouse(9,12);
					}
					if(lo.hasCloud){
						instaCloud(6,3);
					}
					if(lo.hasCave)
					instaCave(11, 15);
					break;
				case Cave:
					CurrentLevelType = LevelType.Cave;
					lo = LevelOptions.getOptions(LevelType.Cave);
					super.menInit();
					cust_Air = Block.stoneInside;
					Main.li.fixStairs();
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}

					for (int y = 1; y < 15; y++) {
						for (int x = 1; x < 15; x++) {
							blocks[x][y] = Block.stoneInside;
						}
					}
					if (lo.hasOres) {
						for (int y = 0; y < 5; y++) {
							blocks[new Random().nextInt(16)][new Random()
									.nextInt(16)] = Block.oreInside[Block.ORE_IRON];
						}
						for (int y = 0; y < 5; y++) {
							blocks[new Random().nextInt(16)][new Random()
									.nextInt(16)] = Block.oreInside[Block.ORE_GOLD];
						}
						for (int y = 0; y < 5; y++) {
							blocks[new Random().nextInt(16)][new Random()
									.nextInt(16)] = Block.oreInside[Block.ORE_DIAMOND];
						}
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					for (int x = 0; x < 16; x++) {
						blocks[x][15] = Block.stone;
					}
					// super.textureInit();
					for (int x = 0; x < 16; x++) {
						blocks[x][0] = Block.stone;
					}
					for (int y = 0; y < 16; y++) {
						blocks[0][y] = Block.stone;
					}
					for (int y = 0; y < 16; y++) {
						blocks[15][y] = Block.stone;
					}

					break;
				case Sand:
					super.menInit();
					cust_Air = Block.air;
					for (int y = 0; y < 16; y++) {
						for (int x = 0; x < 16; x++) {
							blocks[x][y] = Block.air;

						}
					}
					for(int count = 0;count < 16;count++){
						blocks[count][15] = Block.sand;
					}
					for(int count = 0;count < 16;count++){
						blocks[count][14] = Block.sand;
					}
					if(new Random().nextBoolean()){
						blocks[10][13] = Block.cactus;
						blocks[10][12] = Block.cactus;
						blocks[10][11] = Block.cactus;
						blocks[10][10] = Block.cactus;
						blocks[10][9] = Block.cactus;
						}else{
							instaTree(10, 13, 0);
						}
					for (int x = 0; x < 16; x++) {
						blocks[x][16] = Block.air;
					}
					break;
				}
				
return this;
			}
		};

	}

	public static LevelType getRandType() {
		int i = new Random().nextInt();
		if (i % 2 == 0)
			return LevelType.Normal;
		else{
			if(i % 2 == 0){
			return LevelType.Cave;
			
			}else{
				return LevelType.Sand;
			}
			}
		}
}
